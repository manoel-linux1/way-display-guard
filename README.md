# way-display-guard is no longer being maintained

####################################

- way-display-guard-version: sep 2023

- build-latest: 0.0.3

# For FreeBSD

- On FreeBSD, install bash to have no issues.

- To resolve the issue of the way-display-guard error, even with bash, use the following command: `sudo ln -s /usr/local/bin/bash /bin/

####################################

- Support for the distro: Void-Linux/Ubuntu/Debian/Arch/Artix/Manjaro/FreeBSD

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- It can only be executed without superuser and sudo

- It only works on Wayland

- way-display-guard is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with way-display-guard, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of way-display-guard to add additional features.

## Installation

- To install way-display-guard, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/way-display-guard.git

# 2. To install the way-display-guard script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# 3. Execute the way-display-guard script

- `way-display-guard`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The way-display-guard project is currently in development. The latest stable version is 0.0.3. We aim to provide regular updates and add more features in the future.

# License

- way-display-guard is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of way-display-guard.